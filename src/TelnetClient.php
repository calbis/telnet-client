<?php

/**
 * This file is part of graze/telnet-client.
 *
 * Copyright (c) 2016 Nature Delivered Ltd. <https://www.graze.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://github.com/graze/telnet-client/blob/master/LICENSE
 * @link https://github.com/graze/telnet-client
 */

namespace Graze\TelnetClient;

use Exception;
use Graze\TelnetClient\Exception\TelnetException;
use Socket\Raw\Factory as SocketFactory;
use Socket\Raw\Socket;

class TelnetClient implements TelnetClientInterface {

	/**
	 * @var \Socket\Raw\Factory
	 */
	protected $socketFactory;

	/**
	 * @var \Graze\TelnetClient\PromptMatcher
	 */
	protected $promptMatcher;

	/**
	 * @var \Graze\TelnetClient\InterpretAsCommand
	 */
	protected $interpretAsCommand;

	/**
	 * @var string
	 */
	protected $prompt = '\$';

	/**
	 * @var string
	 */
	protected $promptError = 'ERROR';

	/**
	 * @var string
	 */
	protected $lineEnding = "\n";

	/**
	 * @var bool
	 */
	protected $requireLineEndingsOnResponse = true;

	/**
	 * @var \Socket\Raw\Socket
	 */
	protected $socket;

	/**
	 * @var string
	 */
	protected $buffer;

	/**
	 * @var string
	 */
	protected $NULL;

	/**
	 * @var string
	 */
	protected $DC1;

	/**
	 * @var string
	 */
	protected $IAC;

	/**
	 * @param \Socket\Raw\Factory $socketFactory
	 * @param \Graze\TelnetClient\PromptMatcher $promptMatcher
	 * @param \Graze\TelnetClient\InterpretAsCommand $interpretAsCommand
	 */
	public function __construct(
		SocketFactory $socketFactory,
		PromptMatcher $promptMatcher,
		InterpretAsCommand $interpretAsCommand
	) {
		$this->socketFactory = $socketFactory;
		$this->promptMatcher = $promptMatcher;
		$this->interpretAsCommand = $interpretAsCommand;

		$this->NULL = chr(0);
		$this->DC1 = chr(17);
	}

	/**
	 * @param string $dsn
	 * @param string|null $prompt
	 * @param string|null $promptError
	 * @param string|null $lineEnding
	 * @param bool $requireLineEndingsOnResponse
	 * @return void
	 */
	public function connect($dsn, $prompt = null, $promptError = null, $lineEnding = null, $requireLineEndingsOnResponse = true) {
		if ($prompt !== null) {
			$this->setPrompt($prompt);
		}

		if ($promptError !== null) {
			$this->setPromptError($promptError);
		}

		if ($lineEnding !== null) {
			$this->setLineEnding($lineEnding);
		}

		if (is_bool($requireLineEndingsOnResponse)) {
			$this->requireLineEndingsOnResponse = $requireLineEndingsOnResponse;
		}

		try {
			$socket = $this->socketFactory->createClient($dsn);
		} catch (Exception $e) {
			throw new TelnetException(sprintf('unable to create socket connection to [%s]', $dsn), 0, $e);
		}

		$this->setSocket($socket);
	}

	/**
	 * @param string $prompt
	 * @return void
	 */
	public function setPrompt($prompt) {
		$this->prompt = $prompt;
	}

	/**
	 * @param string $promptError
	 * @return void
	 */
	public function setPromptError($promptError) {
		$this->promptError = $promptError;
	}

	/**
	 * @param string $lineEnding
	 * @return void
	 */
	public function setLineEnding($lineEnding) {
		$this->lineEnding = $lineEnding;
	}

	/**
	 * @param \Socket\Raw\Socket $socket
	 * @return void
	 */
	public function setSocket(Socket $socket) {
		$this->socket = $socket;
	}

	/**
	 * @return \Socket\Raw\Socket
	 */
	public function getSocket() {
		return $this->socket;
	}

	/**
	 * @param string $command
	 * @param string|null $prompt
	 * @param string|null $lineEnding
	 *
	 * @param bool $expectTrailingCommand
	 * @return \Graze\TelnetClient\TelnetResponseInterface
	 * @throws \Graze\TelnetClient\Exception\TelnetExceptionInterface
	 */
	public function execute($command, $prompt = null, $lineEnding = null, $expectTrailingCommand = false) {
		if (!$this->socket) {
			throw new TelnetException('attempt to execute without a connection - call connect first');
		}

		$this->write($command);
		return $this->getResponse($prompt, $lineEnding, $expectTrailingCommand);
	}

	/**
	 * @param string|null $prompt
	 * @param string|null $lineEnding
	 *
	 * @param bool $expectTrailingCommand
	 * @return \Graze\TelnetClient\TelnetResponseInterface
	 * @throws \Graze\TelnetClient\Exception\TelnetExceptionInterface
	 */
	public function read($prompt = null, $lineEnding = null, $expectTrailingCommand = false) {
		return $this->getResponse($prompt, $lineEnding, $expectTrailingCommand);
	}

	/**
	 * @param string $command
	 *
	 * @return void
	 * @throws \Graze\TelnetClient\Exception\TelnetExceptionInterface
	 */
	protected function write($command) {
		try {
			$this->socket->write($command . $this->lineEnding);
		} catch (Exception $e) {
			throw new TelnetException(sprintf('failed writing to socket [%s]', $command), 0, $e);
		}
	}

	/**
	 * @param string|null $prompt
	 * @param string|null $lineEnding
	 *
	 * @param bool $expectTrailingCommand
	 * @return \Graze\TelnetClient\TelnetResponseInterface
	 * @throws \Graze\TelnetClient\Exception\TelnetExceptionInterface
	 */
	protected function getResponse($prompt = null, $lineEnding = null, $expectTrailingCommand = false) {
		$prompt = (isset($prompt) ? $prompt : $this->prompt);
		if (!isset($lineEnding)) {
			if ($this->requireLineEndingsOnResponse) {
				$lineEnding = $this->lineEnding;
			}
		}

		$isError = false;
		$buffer = '';
		do {
			// process one character at a time
			try {
				$character = $this->socket->read(1);
			} catch (Exception $e) {
				throw new TelnetException('failed reading from socket', 0, $e);
			}

			if ($character == $this->NULL) {
				continue;
			}

			if ($character == $this->DC1) {
				break;
			}

			if ($this->interpretAsCommand->interpret($character, $this->socket)) {
				continue;
			}

			$buffer .= $character;

			// check for prompt
			if ($this->promptMatcher->isMatch($prompt, $buffer, $lineEnding)) {
				break;
			}

			// check for error prompt
			if ($this->promptMatcher->isMatch($this->promptError, $buffer, $lineEnding, '%s%s')) {
				$isError = true;
				break;
			}

		} while (true);

		if ($expectTrailingCommand) {
			try {
				$character = $this->socket->read(1);
				if (strlen($character) > 0) {
					$this->interpretAsCommand->interpret($character, $this->socket);
				}
			} catch (Exception $e) {
				throw new TelnetException('failed to process expected trailing command', 0, $e);
			}
		}

		return new TelnetResponse(
			$isError,
			$this->trimLineEndings($this->promptMatcher->getResponseText(), $this->lineEnding),
			$this->trimLineEndings($this->promptMatcher->getMatches(), $this->lineEnding)
		);
	}

	/**
	 * @param string|array $item
	 * @param string $lineEnding
	 * @return array|string
	 */
	private function trimLineEndings($item, $lineEnding) {
		if (is_array($item)) {
			array_walk_recursive($item, function (&$trimee) use ($lineEnding) {
				$trimee = trim($trimee, $lineEnding);
			});

			return $item;
		}

		return trim($item, $lineEnding);
	}

	/**
	 * @return \Graze\TelnetClient\TelnetClientInterface
	 */
	public static function factory() {
		return new static(
			new SocketFactory(),
			new PromptMatcher(),
			new InterpretAsCommand()
		);
	}

	public function __destruct() {
		if (!$this->socket) {
			return;
		}

		$this->socket->close();
	}

}
