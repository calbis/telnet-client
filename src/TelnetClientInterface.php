<?php

/**
 * This file is part of graze/telnet-client.
 *
 * Copyright (c) 2016 Nature Delivered Ltd. <https://www.graze.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://github.com/graze/telnet-client/blob/master/LICENSE
 * @link https://github.com/graze/telnet-client
 */

namespace Graze\TelnetClient;

interface TelnetClientInterface {
	/**
	 * @param string $dsn
	 * @param string|null $prompt
	 * @param string|null $promptError
	 * @param string|null $lineEnding
	 * @param bool $requireLineEndingsOnResponse
	 * @return void
	 */
	public function connect($dsn, $prompt = null, $promptError = null, $lineEnding = null, $requireLineEndingsOnResponse = true);

	/**
	 * @param string $command
	 * @param string|null $prompt
	 * @param string|null $lineEnding
	 * @param bool|null $expectTrailingCommand
	 *
	 * @return \Graze\TelnetClient\TelnetResponseInterface
	 */
	public function execute($command, $prompt = null, $lineEnding = null, $expectTrailingCommand = false);

	/**
	 * @return \Socket\Raw\Socket
	 */
	public function getSocket();

	/**
	 * @param string|null $prompt
	 * @param string|null $lineEnding
	 * @param bool|null $expectTrailingCommand
	 *
	 * @return \Graze\TelnetClient\TelnetResponseInterface
	 */
	public function read($prompt = null, $lineEnding = null, $expectTrailingCommand = false);

}
